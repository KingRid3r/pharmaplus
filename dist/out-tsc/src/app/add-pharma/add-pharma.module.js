import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddPharmaPage } from './add-pharma.page';
var routes = [
    {
        path: '',
        component: AddPharmaPage
    }
];
var AddPharmaPageModule = /** @class */ (function () {
    function AddPharmaPageModule() {
    }
    AddPharmaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddPharmaPage]
        })
    ], AddPharmaPageModule);
    return AddPharmaPageModule;
}());
export { AddPharmaPageModule };
//# sourceMappingURL=add-pharma.module.js.map