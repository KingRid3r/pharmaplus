import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
var AddPharmaPage = /** @class */ (function () {
    function AddPharmaPage(nav, modalController, geolocation) {
        this.nav = nav;
        this.modalController = modalController;
        this.geolocation = geolocation;
    }
    AddPharmaPage.prototype.ngOnInit = function () {
    };
    AddPharmaPage.prototype.close = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.dismiss()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AddPharmaPage.prototype.locate = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            _this.data = 'Lat: ' + resp.coords.latitude + '<br/> Long: ' + resp.coords.longitude;
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    AddPharmaPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-pharma',
            templateUrl: './add-pharma.page.html',
            styleUrls: ['./add-pharma.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, ModalController, Geolocation])
    ], AddPharmaPage);
    return AddPharmaPage;
}());
export { AddPharmaPage };
//# sourceMappingURL=add-pharma.page.js.map