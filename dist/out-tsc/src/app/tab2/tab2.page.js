import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import * as Leaf from 'leaflet';
import { NavController, ModalController } from '@ionic/angular';
import { AddPharmaPage } from '../add-pharma/add-pharma.page';
import { HttpClient } from '@angular/common/http';
var Tab2Page = /** @class */ (function () {
    function Tab2Page(http, nav, modalController) {
        this.http = http;
        this.nav = nav;
        this.modalController = modalController;
    }
    Tab2Page.prototype.ngOnInit = function () {
        var map = Leaf.map('leafmap').setView([50.6311634, 3.0599573], 12);
        Leaf.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: 'Pharmacies'
        }).addTo(map);
        this.http.get('http://localhost:3000/pharmacies').subscribe(function (data) {
            try {
                Leaf.geoJSON(data).addTo(map);
            }
            catch (e) {
                console.log('invalid API!', e);
            }
        });
    };
    Tab2Page.prototype.openModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: AddPharmaPage
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.present();
                        console.log('message : je suis nul en web !');
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab2Page = tslib_1.__decorate([
        Component({
            selector: 'app-tab2',
            templateUrl: 'tab2.page.html',
            styleUrls: ['tab2.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient, NavController, ModalController])
    ], Tab2Page);
    return Tab2Page;
}());
export { Tab2Page };
//# sourceMappingURL=tab2.page.js.map