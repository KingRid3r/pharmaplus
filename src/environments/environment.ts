// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost',
  apiPort: '8100',
  firebase : {
    apiKey: "AIzaSyBh1eQmAnbgOMCgnvcs5XHSNt66vFyF1uI",
    authDomain: "mspr-ionic.firebaseapp.com",
    databaseURL: "https://mspr-ionic.firebaseio.com",
    projectId: "mspr-ionic",
    storageBucket: "mspr-ionic.appspot.com",
    messagingSenderId: "746046036198",
    appId: "1:746046036198:web:24a5400e326f55f3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
