import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPharmaciePage } from './info-pharmacie.page';

describe('InfoPharmaciePage', () => {
  let component: InfoPharmaciePage;
  let fixture: ComponentFixture<InfoPharmaciePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPharmaciePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPharmaciePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
