import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InfoPharmaciePage } from './info-pharmacie.page';

const routes: Routes = [
  {
    path: '',
    component: InfoPharmaciePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InfoPharmaciePage]
})
export class InfoPharmaciePageModule {}
