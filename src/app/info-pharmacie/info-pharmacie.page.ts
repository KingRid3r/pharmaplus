import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../services/database.service';

@Component({
  selector: 'app-info-pharmacie',
  templateUrl: './info-pharmacie.page.html',
  styleUrls: ['./info-pharmacie.page.scss'],
})
export class InfoPharmaciePage implements OnInit {
  name;
  id;
  public response = [];
  public listResponse : any;

  constructor(
    public databaseService: DatabaseService)
    {}


  ngOnInit() {
    console.log(`${this.name}`);
            // TODO : REPLACE BY getListResponse
    console.log(`${this.id}`);
    //this.databaseService.

    this.databaseService.getListResponseWithId(this.id, (err, res)=>{
      if (err){
        console.error(err);
      }
      console.log("res : ",res);
      this.listResponse = res;
      //console.log("listReponse = ",this.listResponse);
      this.response = res[0].reponses;
      
    });


    console.log("reponse = ", this.response);
    
  }


}
