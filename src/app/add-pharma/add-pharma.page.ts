import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController } from '@ionic/angular';
import { DatabaseService } from '../services/database.service';

@Component({
  selector: 'app-add-pharma',
  templateUrl: './add-pharma.page.html',
  styleUrls: ['./add-pharma.page.scss'],
})
export class AddPharmaPage implements OnInit {

  pharma = {
    name: '',
    adresse: '',
    longitude:'',
    latitude:''
  };
  


  constructor(private nav: NavController,
    private modalController: ModalController,
    private geolocation : Geolocation,
    public toastController: ToastController,
    public dbs : DatabaseService
    ){ 
      this.pharma.name = "";
   }

  ngOnInit() {
  }

  
  async close(){
    await this.modalController.dismiss(); 
  }

/*    ###################################### No longer usefull ######################################
  checked : boolean = false;
  addValue(e): void {
    var isChecked = e.currentTarget.checked;
    if(!isChecked){
      this.geolocation.getCurrentPosition().then((resp) => {
        
        //this.latitude =  resp.coords.latitude.toString()
        //this.longitude =  resp.coords.longitude.toString()
       }).catch((error) => {
         console.log('Error getting location', error);
       });

    }
    else{
      //this.latitude = ""
      //this.longitude = ""

    }
  }
*/

  AddPharma(){
    this.dbs.addPharmacie(this.pharma);
    console.log(this.pharma);
    this.modalController.dismiss();
  }


}
