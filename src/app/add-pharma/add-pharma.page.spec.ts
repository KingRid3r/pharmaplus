import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPharmaPage } from './add-pharma.page';

describe('AddPharmaPage', () => {
  let component: AddPharmaPage;
  let fixture: ComponentFixture<AddPharmaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPharmaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPharmaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
