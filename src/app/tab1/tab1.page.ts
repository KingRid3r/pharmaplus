import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  PAB: number;
  PAN: number;
  TDR: number;
  PVN: number;
  CM: number;

  constructor() {

  }

  calcul_PAN() {
    console.log('calcul pan');
    if (this.PAB > 0 && this.TDR > 0){
      this.PAN = this.PAB * (1 - this.TDR);
      console.log('dans le if');
      console.log('Pan: ' + this.PAN);
    } else {
      console.log('dans le else');
    }
  }

  calcul_TDR() {
    console.log('calcul TDR');
    if (this.PAB > 0 && this.PAN > 0) {
      this.TDR = ( (1 - this.PAN) / this.PAB) * 100;
      console.log('TDR: ' + this.TDR);
      console.log('calcul : ' + '((1 - ' + this.PAN + ') / ' + this.PAB + ') * 100');
    }
  }

  calcul_PVN() {
    console.log('calcul pan');
    if (this.PAN > 0 && this.CM > 0) {
      this.PVN = this.PAN * this.CM;
    }
  }

  calcul_CM() {
    console.log('calcul pan');
    if (this.PVN > 0 && this.PAN > 0) {
      this.CM = this.PVN / this.PAN;
    }
  }

  calcul() {
    this.calcul_PAN();
    this.calcul_TDR();
    this.calcul_PVN();
    this.calcul_CM();
  }

  clear() {
    this.PAB = null;
    this.PAN = null;
    this.TDR = null;
    this.PVN = null;
    this.CM = null;
  }


}
