import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {CreateFormPage} from '../create-form/create-form.page';
import {DatabaseService} from '../services/database.service';
import {FillFormPage} from '../fill-form/fill-form.page';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  formulaires = [];
  constructor(public modalController: ModalController, private db: DatabaseService) {}

  ngOnInit(){
    this.db.OnListenFormulaires().subscribe(val => {
      this.formulaires = [];
      val.forEach((obj)=>{
        let data = obj.payload.doc.data();
        // @ts-ignore
        data.id = obj.payload.doc.id;
        this.formulaires.push(data);
      })
    });
  }

  async editFormulaire(id? :String) {
    const modal = await this.modalController.create({
      component: CreateFormPage,
      componentProps: {
        'id': id,
      }
    });
    modal.present();
  }

  async fillFormulaire(id: String){
    const modal = await this.modalController.create({
      component: FillFormPage,
      componentProps: {
        'id': id,
      }
    });
    modal.present();
  }

  deleteFormulaire(id): void{
    this.db.deleteFormulairesWithId(id);
  }
}
