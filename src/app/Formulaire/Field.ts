export class Field {
    upLabel = '';
    name = '';
    type = 'abstract';
    constructor(doc) {
        if (doc.upLabel) {
            this.upLabel = doc.upLabel;
        }
        if (doc.name) {
            this.name = doc.name;
        }
        if (doc.type) {
            this.type = doc.type;
        }
    }
}
