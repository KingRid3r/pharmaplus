import {Field} from '../Field';

export class InputText extends Field {
    placeholder = '';
    type = 'InputText';
    constructor(doc) {
        super(doc);
        if (doc.placeholder){
            this.placeholder = doc.placeholder;
        }
    }

    getDoc() {
       return{
           placeholder: this.placeholder,
           upLabel: this.upLabel,
           type: this.type
       };
    }
}
