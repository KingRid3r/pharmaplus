import {Field} from '../Field';

export class InputSelect extends Field {
    type = 'InputSelect';
    propositions = [];
    constructor(doc) {
        super(doc);
        this.propositions = doc.propositions;
    }
}
