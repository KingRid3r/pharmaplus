import { Injectable } from '@angular/core';
import { InputText } from './Fields/InputText';
import {InputSelect} from './Fields/InputSelect';
import { Field } from './Field';
import {DatabaseService} from '../services/database.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class FormulaireService {
  fields = [];
  name = '';
  id = '';
  private subscription: any;

  constructor(private db: DatabaseService) {
  }

  setDoc(id?: string ): void{
    if (id){
      this.subscription = this.db.OnListenFormulaire(id).subscribe(val => {
        this.fields = [];
        const data = val.payload.data();
        this.id = val.payload.id;
        // @ts-ignore
        if (data && data.fields){
          // @ts-ignore
          data.fields.forEach((f) => {
            this.fields.push(new FormulaireService.FIELDS[f.type](f));
          });
        }
        // @ts-ignore
        if (data && data.name){
          // @ts-ignore
          this.name = data.name;
        }
      });
    }
  }


  addField(field: Field) {
    this.fields.push(field);
  }

  static get FIELDS() {
    return {
      InputText,
      InputSelect
    };
  }

  getDoc() {
    return {
      fields: this.fields.map((f) => {
        return f.getDoc();
      }),
      name: this.name
    };
  }



  reset(){
    if (this.subscription){
      this.subscription.unsubscribe();
    }
    this.fields = [];
    this.name = '';
    this.id = '';
  }

  save() {
    if (this.id !== '') {
      console.log(this.fields);
      this.db.updateFormulaire(this.id ,this.getDoc()).then((res) => {
        console.log(res);
        console.log('update', this.id);
      }, (err) => {
        console.error(err);
      });
    } else {
      this.db.insertFormulaire(this.getDoc()).then((res) => {
        console.log(res);
        this.id = res.id;
        this.setDoc(this.id);
        console.log('insert', this.id);
      }, (err) => {
        console.error(err);
      });

    }
  }

}
