import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'info-pharmacie', loadChildren: './info-pharmacie/info-pharmacie.module#InfoPharmaciePageModule' },
  { path: 'create-form', loadChildren: './create-form/create-form.module#CreateFormPageModule' },  { path: 'fill-form', loadChildren: './fill-form/fill-form.module#FillFormPageModule' }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
