import {Component, Input, OnInit} from '@angular/core';
import {DatabaseService} from '../services/database.service';
import {FormulaireService} from '../Formulaire/formulaire.service';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-fill-form',
  templateUrl: './fill-form.page.html',
  styleUrls: ['./fill-form.page.scss'],
})
export class FillFormPage implements OnInit {
  @Input() id?: string;
  pharmalist = [];
  pourQui = '';
  reponses = {};
  constructor(private db: DatabaseService, public formServ: FormulaireService, public modalController: ModalController) { }

  reponse(test){
    console.log(test);
  }

  ngOnInit() {
    this.formServ.reset();
    this.formServ.setDoc(this.id);
    this.db.getPharmacieList((err, res)=>{
      if (err){
        console.error(err);
      }
      this.pharmalist = res;
      console.log(res);
    })
  }

  submit(){
    console.log(this.reponses);
    let doc = {pourQui: this.pourQui, avecQuoi: this.id, reponses:[]};
    for (let prop in this.reponses) {
      doc.reponses.push({label: prop, value : this.reponses[prop]})
    }
    this.db.insertReponseFormulaires(doc);
    this.modalController.dismiss();

  }

}
