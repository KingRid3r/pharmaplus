import {Component, Input, OnInit} from '@angular/core';
import {FormulaireService} from '../Formulaire/formulaire.service';
import {DatabaseService} from '../services/database.service';
import {from, Observable} from 'rxjs';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.page.html',
  styleUrls: ['./create-form.page.scss'],
})
export class CreateFormPage implements OnInit {
  @Input() id?: string;


  constructor(private db: DatabaseService, public formServ: FormulaireService, public modalController: ModalController) {}

  ngOnInit() {
    this.formServ.reset();
    this.formServ.setDoc(this.id);
  }

  addTextInput() {
    this.formServ.fields.push(new FormulaireService.FIELDS.InputText({}));
    console.log(this.formServ.fields);
  }

  addSelectInput() {
    this.formServ.fields.push(new FormulaireService.FIELDS.InputSelect({}));
    console.log(this.formServ.fields);
  }

  removeField(index){
    this.formServ.fields.splice(index, 1);
  }

  async saveForm() {
    this.formServ.save();
    this.modalController.dismiss();
  }
}
