import {Injectable} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {flatMap} from 'rxjs/internal/operators';



@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

  constructor(public db: AngularFirestore) {
      // test code
  }


      addUser(value) {
        return new Promise<any>((resolve, reject) => {
          this.db.collection('/pharma').add(value)
          .then(
            (res) => {
              resolve(res);
            },
            err => reject(err)
          );
        });
      }

    insertPharmacie(value) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/pharmacie').add(value)
                .then(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        console.log('erreur : ', err);
                        err => reject(err);
                    }
                );
        });
    }

    insertFormulaire(value) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/formulaires').add(value)
                .then(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        console.log('erreur : ', err);
                        err => reject(err);
                    }
                );
        });
    }

    insertReponseFormulaires(value) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/reponseFormulaires').add(value)
                .then(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        console.log('erreur : ', err);
                        err => reject(err);
                    }
                );
        });
    }


    updateFormulaire(id ,value) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/formulaires').doc(id).update(value)
                .then(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        console.log('erreur : ', err);
                        err => reject(err);
                    }
                );
        });
    }

    insertData(value) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/pharmacie').add(value)
                .then(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        console.log('erreur : ', err);
                        err => reject(err);
                    }
                );
        });
    }

    addPharmacie(p) {
        console.log(p);
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/pharmacie').add(p)
                .then(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        console.log('erreur : ', err);
                        err => reject(err);
                    }
                );
        });
    }

    public PharmaList: any[];
    public Pharma: any[];


    getPharmacieList(cb) {
           this.db.collection('pharmacie').snapshotChanges().subscribe(val => {
               const pharmas = [];
               val.forEach((obj)=>{
                   let data = obj.payload.doc.data();
                   // @ts-ignore
                   data.id = obj.payload.doc.id;
                   pharmas.push(data);
               });
               cb(null, pharmas);
           });
    };



    getPharma(name) {
        this.db.collection('pharmacie').valueChanges().subscribe((res) => {
            this.PharmaList = res;
            this.Pharma = this.PharmaList.find(res => res.name === name);
            //console.log("pharma : ",this.Pharma);
        });
    }

    OnListenFormulaire(id: string) {
        return this.db.collection('/formulaires').doc(id).snapshotChanges()
        }

    OnListenPharmacie(id: string) {
            return this.db.collection('/pharmacie').doc(id).snapshotChanges()
    }

    OnListenFormulaires() {
        return this.db.collection('/formulaires').snapshotChanges();
    }

    deleteFormulairesWithId(id){
        this.db.collection('/formulaires').doc(id).delete().catch((e)=>{
            console.error(e);
        })
    }

    getListResponseWithId(id, cb){
        this.db.collection('reponseFormulaires', ref => ref.where('pourQui', '==', id)).snapshotChanges().subscribe(val => {
            const reponses = [];
            val.forEach((obj)=>{
                let data = obj.payload.doc.data();
                // @ts-ignore
                data.id = obj.payload.doc.id;
                reponses.push(data);
            });
            cb(null, reponses);
        });
    }
}

//this.bankAccountsCollection = db.collection('/reponseFormulaires', ref => ref.where('pourQui', '==', element.id));
