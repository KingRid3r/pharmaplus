import {Component, OnInit, ViewChild} from '@angular/core';
import * as Leaf from 'leaflet';
import { NavController, ModalController, NavParams } from '@ionic/angular';
import { AddPharmaPage } from '../add-pharma/add-pharma.page';
import {HttpClient} from '@angular/common/http';
import {GeoJSON} from 'geojson';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {InfoPharmaciePage} from '../info-pharmacie/info-pharmacie.page';
import {environment} from '../../environments/environment';

import { AngularFirestore } from '@angular/fire/firestore';
import { DatabaseService } from '../services/database.service';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
    customMarker = new Leaf.Icon({
    iconSize: [37, 52],
    iconAnchor: [23, 47],
    iconUrl: '/assets/icon/pharmaIcon.png'
  });

  public goalList: any;
  public tab = []; 
  public loadedGoalList: any;
  public geoJson: any;
  map: any;

  constructor(public http: HttpClient,
              private nav: NavController,
              private modalController: ModalController,
              private geolocation: Geolocation,
              private firestore: AngularFirestore,
              public databaseService: DatabaseService){

              }

  ngOnInit() {

          // GET ALL PHARMACIES
      this.geolocation.getCurrentPosition().then((resp) => {
      this.initMap(resp.coords.latitude, resp.coords.longitude);
        this.databaseService.getPharmacieList((err, res)=>{
          if (err){
            console.error(err);
          }
          this.geoJson = this.addListToMap(res);
          this.loadedGoalList = res;
          this.goalList = res;
        });
    }).catch((error) => {
      console.log('Error getting location', error);
      this.initMap(48.8534, 2.3488);
    });

  }


  initMap(latitude: number, longitude: number) {
    this.map = Leaf.map('leafmap').setView([latitude, longitude], 12);
    Leaf.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '©PharmaPlus',
      maxZoom: 18
    }).addTo(this.map);

    this.http.get(environment.apiUrl + ':' + environment.apiPort + '/pharmacies').subscribe(data => {
        this.addGeoJsonToMap(data);
      }, err=>{
        console.log("API DOWN!", err);
      });

    setTimeout(() => { this.map.invalidateSize(); }, 1);
  }

  async openInfoModal(props) {
    console.log("props : ", props);
    const modal = await this.modalController.create({
      component: InfoPharmaciePage,
      componentProps: props
    });
    modal.present();
  }

  getPropsFromLayer(layer){
    console.log("layer : ",layer);
    const props = {
      name: 'No Name',
      id : null
    };
    if (layer.feature.properties.name) {
      props.name = layer.feature.properties.name;
      props.id = layer.feature.properties._id;
    }
    return props;
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: AddPharmaPage
    });
    modal.present();
    console.log('message : je suis nul en web !');
  }

  search(event) {

  }

  initializeItems(): void{
    this.goalList = this.loadedGoalList;
  }

  shearch(evt) {
    this.tab = [];
    //this.initializeItems();
    const searchItem = evt.target.value;
    console.log("search : ", searchItem);
    this.goalList.forEach(element => {
      //console.log(element);
      if(element.name.indexOf(searchItem) !== -1 ){
        console.log(element);
        this.tab.push(element);
      }
    });
    console.log("finally = ", this.tab);

    if(searchItem == ""){
      console.log("vide");
      this.tab = [];
    }

  }

  addListToMap(list){
    console.log(list);

    const geojson = {
      type: "FeatureCollection",
      features: []
    };
    list.forEach((pharma)=>{
      geojson.features.push({
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [
            pharma.longitude,
            pharma.latitude
          ]
        },
        properties: {
          amenity: 'pharmacy',
          ref_fr_finess: '',
          opening_hours: '',
          geo_point_2d: [
            pharma.latitude,
            pharma.longitude
          ],
          _id: pharma.id,
          dispensing: 'yes',
          name: pharma.name
        }
      })
    });
    this.addGeoJsonToMap(geojson);
  }


  addGeoJsonToMap(geojson){
    try {
      let geoJson = new Leaf.GeoJSON(geojson as GeoJSON, {
        pointToLayer: (feature, latlng) => {
          return Leaf.marker(latlng, {icon: this.customMarker});
        },
        onEachFeature: (feature, layer) => {

          layer.on('click', async (e) => {
            // e = event
            console.log(layer);
            await this.openInfoModal(this.getPropsFromLayer(layer));
            // You can make your ajax call declaration here
          });
        }
      });
      geoJson.addTo(this.map);
    } catch (e) {
      console.error(e);
    }
  }


  findPharmaByName(props){
    
    this.openInfoModal(props);
    this.map.setView([props.latitude, props.longitude], 15);
  }


}
